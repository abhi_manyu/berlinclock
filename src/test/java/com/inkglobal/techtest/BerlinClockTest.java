package com.inkglobal.techtest;

import org.junit.Assert;
import org.junit.Test;

public class BerlinClockTest {

	BerlinClock berlinClock = new BerlinClock();

	// Test to verify that top hour and bottom hour rows have 4 lamps each
	@Test
	public void testTopAndBottomHourRows_ShouldHave4Lamps() {
		Assert.assertEquals(4, berlinClock.getTopRowHourString(15).length());
		Assert.assertEquals(4, berlinClock.getTopRowHourString(15).length());
	}

	// Test to verify that top hour row lights a red lamp for every 5 hours
	@Test
	public void testTopHourRow_LightsRedLampForEvery5Hours() {
		Assert.assertEquals("RRRO", (berlinClock.getTopRowHourString(18)));
		Assert.assertEquals("RROO", (berlinClock.getTopRowHourString(14)));
	}

	// Test to verify that bottom hour row lights a red lamp for every hour left
	// from top row
	@Test
	public void testBottomHourRow_LightsRedLampForEveryHourLeftFromTopRow() {
		Assert.assertEquals("RRRO", (berlinClock.getBottomRowHourString(18)));
		Assert.assertEquals("RROO", (berlinClock.getBottomRowHourString(7)));
	}

	@Test
	public void testComplete_MinuteString() {
		Assert.assertEquals("YYRYYRYYRYY YYYO",
				(berlinClock.getTopRowMinuteString(58) + " " + berlinClock
						.getBottomRowMinuteString(58)));
		Assert.assertEquals("YYROOOOOOOO YYOO",
				(berlinClock.getTopRowMinuteString(17) + " " + berlinClock
						.getBottomRowMinuteString(17)));
	}

	// Test to verify top minute row has 11 lamps
	@Test
	public void testTopMinute_ShouldHave11Lamps() {
		Assert.assertEquals(11, berlinClock.getTopRowMinuteString(15).length());
	}

	// Test to verify bottom minute row has 4 lamps
	@Test
	public void testBottomMinute_ShouldHave4Lamps() {
		Assert.assertEquals(4, berlinClock.getBottomRowMinuteString(15)
				.length());
	}

	// Test to verify top minute row lights a yellow lamp for every 5 minutes
	// apart from 3rd, 6th and 9th lamp
	@Test
	public void testTopRow_MinuteString() {
		Assert.assertEquals("YYROOOOOOOO",
				berlinClock.getTopRowMinuteString(15));
		Assert.assertEquals("YYOOOOOOOOO",
				berlinClock.getTopRowMinuteString(12));
	}

	// Test to verify bottom row minute lights a yellow lamp for every minute
	// left from top row
	@Test
	public void testBottomRow_MinuteString() {
		Assert.assertEquals("YYYO", berlinClock.getBottomRowMinuteString(58));
		Assert.assertEquals("YOOO", berlinClock.getBottomRowMinuteString(11));
	}

	// Test to verify 3rd, 6th and 9th lamp in top minute row are red
	@Test
	public void testTopMinute_3rd6th9thLampColour() {
		String minuteString = berlinClock.getTopRowMinuteString(49);
		Assert.assertEquals("R", minuteString.substring(2, 3));
		Assert.assertEquals("R", minuteString.substring(5, 6));
		Assert.assertEquals("O", minuteString.substring(9, 10));
	}

	// Test to verify yellow lamp blinks every 2 seconds
	@Test
	public void testSeconds() {
		Assert.assertEquals("Y", berlinClock.getSeconds(4));
		Assert.assertEquals("O", berlinClock.getSeconds(3));
		Assert.assertNotEquals("Y", berlinClock.getSeconds(13));
	}

	// Test to verify the complete Berlin time equivalent string for a given
	// time
	@Test
	public void testComplete_BerlinTimeString() {
		Assert.assertEquals("Y OOOO OOOO OOOOOOOOOOO OOOO",
				berlinClock.getBerlinTimeEquivalentForAGivenTime("00:00:00"));
		Assert.assertEquals("O RROO RRRO YYROOOOOOOO YYOO",
				berlinClock.getBerlinTimeEquivalentForAGivenTime("13:17:01"));
		Assert.assertEquals("O RRRR RRRO YYRYYRYYRYY YYYY",
				berlinClock.getBerlinTimeEquivalentForAGivenTime("23:59:59"));
		Assert.assertEquals("Y RRRR RRRR OOOOOOOOOOO OOOO",
				berlinClock.getBerlinTimeEquivalentForAGivenTime("24:00:00"));
	}

}
