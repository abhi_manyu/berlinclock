package com.inkglobal.techtest;

import java.util.ArrayList;
import java.util.List;

public class BerlinClock {

	protected String getBerlinTimeEquivalentForAGivenTime(String time) {
		List<Integer> timeSections = new ArrayList<Integer>();
		for (String part : time.split(":")) {
			timeSections.add(Integer.parseInt(part));
		}
		StringBuilder convertedBerlinTime = new StringBuilder("");
		convertedBerlinTime.append(getSeconds(timeSections.get(2)));
		convertedBerlinTime.append(" ");
		convertedBerlinTime.append(getTopRowHourString(timeSections.get(0)));
		convertedBerlinTime.append(" ");
		convertedBerlinTime.append(getBottomRowHourString(timeSections.get(0)));
		convertedBerlinTime.append(" ");
		convertedBerlinTime.append(getTopRowMinuteString(timeSections.get(1)));
		convertedBerlinTime.append(" ");
		convertedBerlinTime.append(getBottomRowMinuteString(timeSections.get(1)));
		return convertedBerlinTime.toString();
	}

	protected String getSeconds(int number) {
		if (number % 2 == 0)
			return "Y";
		else
			return "O";
	}

	protected String getTopRowHourString(int hour) {
		return getLampOnOffString(getNumberOfOnHoursLamp(hour), "R", 4);
	}

	protected String getBottomRowHourString(int hour) {
		return getLampOnOffString((hour % 5), "R", 4);
	}

	protected String getTopRowMinuteString(int minutes) {
		return getLampOnOffString(getNumberOfOnHoursLamp(minutes), "Y", 11).replace("YYY", "YYR");
	}

	protected String getBottomRowMinuteString(int minutes) {
		return getLampOnOffString((minutes % 5), "Y", 4);
	}

	protected String getLampOnOffString(int number, String colourOfLamp, int numberOfLamps) {
		String output = "";

		for (int i = 0; i < number; i++) {
			output += colourOfLamp;
		}

		for (int i = 0; i < (numberOfLamps - number); i++) {
			output += "O";
		}

		return output;
	}

	private int getNumberOfOnHoursLamp(int hours) {
		return (hours - (hours % 5)) / 5;
	}
}
